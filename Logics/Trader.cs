﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs.Host;
using ObudaiFunctions.DTOs;

namespace ObudaiFunctions.Logics
{
    class Trader
    {
        TraceWriter log;

        const string USD = "usd";
        const string BTC = "btc";
        const string ETH = "eth";
        const string XRP = "xrp";
        const double BTC_multiplier = 1;
        const double ETH_multiplier = 1;
        const double XRP_multiplier = 5;

        const string ACTION_PURCHASE = "purchase";
        const string ACTION_SELL = "sell";

        BalanceDTO balance;

        Dictionary<string, ExchangeRateDTO> rates = new Dictionary<string, ExchangeRateDTO>();
        Dictionary<string, double> multipliers;

        static HttpClient client = new HttpClient();
        const string BASEADDRESS = "https://obudai-api.azurewebsites.net/";

        public Trader(TraceWriter log)
        {
            this.log = log;
        }
        internal async Task DoSomeThing()
        {
            try
            {
                log.Info("Job started");
                client.BaseAddress = new Uri(BASEADDRESS);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Add("X-Access-Token", "02837D0A-AB78-413D-9F0E-DA255117E3C4");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                balance = await GetBalance();
                if (balance != null)
                {
                    log.Info(balance.ToString());
                    var res = await Logic();
                    log.Info($"result: {res})");
                }
                log.Info("Job finished");
            }
            catch (Exception x)
            {
                log.Error($"Shit happened:\n{x.Message}");
            }
        }
        private async Task<bool> getRates()
        {
            rates = new Dictionary<string, ExchangeRateDTO>();
            try
            {
                rates.Add(BTC, await GetExchangeRate(BTC));
                rates.Add(ETH, await GetExchangeRate(ETH));
                rates.Add(XRP, await GetExchangeRate(XRP));
                rates[BTC].CurrentAmount = balance.Btc;
                rates[ETH].CurrentAmount = balance.Eth;
                rates[XRP].CurrentAmount = balance.Xrp;
                rates[BTC].Multiplier = BTC_multiplier;
                rates[ETH].Multiplier = ETH_multiplier;
                rates[XRP].Multiplier = XRP_multiplier;
                return true;
            }
            catch (Exception x)
            {
                return false;
            }
        }

        private async Task<String> Logic()
        {
            string result = "";
            bool rOK = await getRates();
            if (rOK)
            {
                foreach (var rate in rates)
                {
                    result += "\n";
                    double d = decision(rate.Value);
                    if (d > 0)
                    {
                        result += await Trade(ACTION_SELL, rate.Value.Symbol, d);
                    }
                    else
                    {
                        result += await Trade(ACTION_PURCHASE, rate.Value.Symbol, Math.Abs(d));
                    }
                }
            }
            log.Info($"Current Rates: \n\t BTC: {rates[BTC].CurrentRate}\n\t ETH: {rates[ETH].CurrentRate}\n\t XRP: {rates[XRP].CurrentRate}");
            return result;
        }
        private double decision(ExchangeRateDTO rate)
        {
            double purchased = rate.CurrentAmount == 0 ? 0 : rate.CurrentAmount / rate.Multiplier; //how many times we purchased
            double counter = 0;
            foreach (var oldRate in rate.History)
            {
                if (purchased >= counter)
                {
                    if (oldRate.Value >= rate.CurrentRate)
                    {
                        counter--;
                        break;
                    }
                    counter++;
                }
                else
                {
                    break;
                }
            }
            return counter == 0 ? -1 * rate.Multiplier : counter * rate.Multiplier;
        }
        private async Task<ExchangeRateDTO> GetExchangeRate(string symbol)
        {
            ExchangeRateDTO exc = null;
            HttpResponseMessage response = await client.GetAsync($"api/exchange/{symbol}");
            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                exc = Newtonsoft.Json.JsonConvert.DeserializeObject<ExchangeRateDTO>(json);
            }
            return exc;
        }
        private async Task<BalanceDTO> GetBalance()
        {
            BalanceDTO blnc = null;
            HttpResponseMessage response = await client.GetAsync("api/account");
            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                blnc = Newtonsoft.Json.JsonConvert.DeserializeObject<BalanceDTO>(json);
            }
            return blnc;
        }
        private async Task<String> Trade(string action, string symbol, double amount)
        {
            TradeDTO pur = new TradeDTO(symbol, amount);
            var content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(pur), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(
                 $"api/account/{action}", content);
            if (response.IsSuccessStatusCode)
            {
                return $"Success: {action} - {amount} of {symbol}";
            }
            return $"Error: {action} - {amount} of {symbol}";
        }
    }
}
