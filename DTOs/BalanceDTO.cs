﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObudaiFunctions.DTOs
{
    class BalanceDTO
    {
        public string Token { get; set; }
        public double Usd { get; set; }
        public double Btc { get; set; }
        public double Eth { get; set; }
        public double Xrp { get; set; }
        public override string ToString()
        {
            return $"Balance: \n USD: {Usd}\n BTC: {Btc}\n ETH: {Eth}\n XRP: {Xrp}";
        }
    }
}
