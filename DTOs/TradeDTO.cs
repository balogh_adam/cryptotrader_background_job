﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObudaiFunctions.DTOs
{
    class TradeDTO
    {
        public TradeDTO(string symbol, double amount)
        {
            Symbol = symbol;
            Amount = amount;
        }
        public string Symbol { get; set; }
        public double Amount { get; set; }
    }
}
